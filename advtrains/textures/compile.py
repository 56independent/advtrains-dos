# Make a file. This file should contain the name of your svg files nd the operations you wish to perform. Put it into ARGV[1]
# f1>f2      - Compile SVG to png file (optional f2 argument)
# f1+f2    - merge two files, new filename f1f2
# f1*r     - Make a series of x degree rotations, default advtrains-compatible, again and again until passed 359 degrees. Define using ARGV[2]
# f1*x     - Rotate by x degrees
# f1#x     - Change colour to given hex value

rotationAmount = 45 # In degrees

import sys

if 4 <= len(sys.argv):
    rotationAmount = sys.argv[2]

# AST generation

operators = {
    "merge":"+",
    "rotateDegrees":"*",
    "rotateFull":"*r",
    "export":">"
    }

with open(sys.argv[1]) as op:
    operations = op.read()

operations = operations.split("\n")

for i in range(len(operations)):
    for o in operators:
        operations[i] = operations[i].replace(operators[o], ".splithere." + o + ".splithere.")
    operations[i] = operations[i].split(".splithere.")

# AST Parsing
def rotate(degree, filename):
    import svgutils.transform as trans
    figure = trans.fromfile(filename + ".svg")
    figure.rotate(degree)
    figure.save(filename)

def recurseRotate(filename):
    for i in range(int(360/rotateAmount)):
        rotate(rotateAmount*i, filename)

def merge(file1, file2):
    figure1 =  trans.fromfile(file1 + ".svg")
    figure2 =  trans.fromfile(file2 + ".svg")

def export(file1, file2):
    import os

    os.system("inkscape --export-png=" + file2 + ".png " + file1 + ".svg")

for i in range(len(operations)):
    ops = operations[i]
    try:
        if ops[1] == "merge":
            merge(ops[0], ops[2])
        elif ops[1] == "rotateDegrees":
            rotate(ops[2], ops[1])
        elif ops[1] == "rotateFull":
            recurseRotate(ops[0])
        elif ops[1] == "export":
            if len(ops) < 3:
                ops[2] = ops[1]
            export(ops[0], ops[2])
    except:
        pass
